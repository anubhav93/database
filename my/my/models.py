from django.db import models

class s(models.Model):
	sn=models.CharField(max_length =20)
	pn=models.CharField(max_length =20)
	desc=models.TextField()

	def __unicode__(self):
		return self.sn

class lang(models.Model):
	name=models.ForeignKey(s)
	Age=models.IntegerField()

	def __unicode__(self):
		return str(self.Age)